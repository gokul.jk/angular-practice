import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

import { DirectivesComponent } from './directives/directives.component';
import { PipeComponent } from './pipe/pipe.component';
import { UserComponent } from './user/user.component';
@NgModule({
  declarations: [AppComponent,  DirectivesComponent, PipeComponent, UserComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
