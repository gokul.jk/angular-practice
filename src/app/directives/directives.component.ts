import { Component } from '@angular/core';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css'],
})
export class DirectivesComponent {
  user=true;                                 //ngIf
  name1=[{name:'sam'},{name:'gokul'}]         //ngFor
  num='';                                     //ngSwitch
}
