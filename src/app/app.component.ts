import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name="gokul";
  userObject={
    name:'john',
    age:'30',
    id:1
  }
  handleEvent(event:any){
    console.log(event);
  }
}
