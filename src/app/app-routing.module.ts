import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user/user.component';
import { PipeComponent } from './pipe/pipe.component';
import { DirectivesComponent } from './directives/directives.component';

const routes: Routes = [
  {path:'user',component:UserComponent},
  {path:'pipe',component:PipeComponent},
  {path:'directive',component:DirectivesComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
